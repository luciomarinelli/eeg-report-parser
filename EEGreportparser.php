<?php

$dirname="report20200601-20200715/"; //set here the folder where mht reports are located

$tags_match = array( //create an associative array where tag strings (key) within the html template are matched with one word tags (value)
"atten<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"atten", 
"_suppression<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"suppression", 
"5-6Hz<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"56hz", 
"7-8Hz<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"78hz", 
"no gradient<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"gradient", 
"asymmetric<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"asymmetric", 
"discont<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"discont", 
"unreact<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"unreact", 
"brain death<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"death", 
"generalized<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"generalized", 
"_focal<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"focal", 
"multifocal<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"multifocal", 
"_ictal<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"ictal", 
"interictal<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"interictal", 
"S&amp;W<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"sw", 
"_SE<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"se", 
"NCSE<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"ncse", 
"EPC<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"epc", 
"LPD<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"lpd", 
"BIPD<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"bipd", 
"GPD<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"gpd", 
"SIRPID<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"sirpid", 
"burst-suppression<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"burst", 
"TW<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"tw", 
"anoxic<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"anoxic", 
"toxic-metabolic<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"toxic", 
"lesional<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"lesional", 
"TBI<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"tbi", 
"encephalitis<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"encephalitis", 
"dementia<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"dementia", 
"prion disease<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"prion",
"normal variant<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"variant",
"focal slowing<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"slowing",
"rhythmic delta<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"delta", 
"EDB<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"edb", 
"coma<INPUT TYPE=3D\"checkbox\" CHECKED>"=>"coma")
;

if ($dir = opendir($dirname)) {

    while ($file = readdir($dir)) {

        if (($file !== '.') && ($file !== '..')) {
            echo "\r\nTrovato file $file\r\n";
            $file_complete = $dirname.$file;
            $handle = @fopen("$file_complete", "r");

            if ($handle) {
                echo "Aperto file $dirname.$file\r\n";
                $tags = array("filename"=>$file, "atten"=>0, "suppression"=>0, "56hz"=>0, "78hz"=>0, "gradient"=>0, "asymmetric"=>0, "discont"=>0, "unreact"=>0, "death"=>0, "generalized"=>0, "focal"=>0, "multifocal"=>0, "ictal"=>0, "interictal"=>0, "sw"=>0, "se"=>0, "ncse"=>0, "epc"=>0, "lpd"=>0, "bipd"=>0, "gpd"=>0, "sirpid"=>0, "burst"=>0, "tw"=>0, "anoxic"=>0, "toxic"=>0, "lesional"=>0, "tbi"=>0, "encephalitis"=>0, "dementia"=>0, "prion"=>0, "variant"=>0, "slowing"=>0, "delta"=>0, "edb"=>0, "coma"=>0); //zeroes array - keys must match values of $tags_match array
                $CHECKED_included = 0; //reset CHECKED_included counter for this file
                $buffer_prev = ""; //reset
                $buffer_prev_prev = ""; //reset
                $buffer_prev_prev_prev = ""; //reset

                while (!feof($handle)) {
                    $buffer = fgets ($handle); //reads line by line, unsuitable to strip line breaks
                    $buffer = str_replace ("=\r\n", "", $buffer); //strip line breaks preceded by =
                    $buffer = str_replace ("\r\n", "", $buffer); //strip extra line breaks
                    $buffer = str_replace ("\r\n", "", $buffer); //strip extra line breaks 2
                    $buffer_merged = $buffer_prev_prev_prev . $buffer_prev_prev . $buffer_prev . $buffer; //merge buffers
                    $buffer_merged = strip_tags ($buffer_merged, '<INPUT>'); //strip all but <INPUT>

                    foreach ($tags_match as $key => $value) { //search for TAG name within $preCHECKED string
                        if (strpos ($buffer_merged, $key) && ($tags [$value] !== 1)) {
                            $tags [$value] = 1;
                            echo "Trovato $value\r\n";
                            $CHECKED_included++;
                            }

                        }

                        $buffer_prev_prev_prev = $buffer_prev_prev; //stores previous 3 buffers to merge with the next one
                        $buffer_prev_prev = $buffer_prev;
                        $buffer_prev = $buffer;

                    }

                echo "Trovati $CHECKED_included CHECKED\r\n";
                fclose($handle);
                }

                $fp = fopen('export.csv', 'a');
                fputcsv($fp, $tags);
                fclose($fp);

            }
        }
    }

?>

