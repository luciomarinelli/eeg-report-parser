EEG Report Parser - release 20200724

"EEG Report Parser" is PHP script to parse MHTML files including tags in shape of HTML checkboxes <input type="checkbox" /> in order to extract tag values generating a CSV file. This script has been implemented for electroencephalogram (EEG) reports processed by Nicolet NicVue Patient Management Software (Natus Medical Incorporated) but can be potentially adapted for other EEG software.
A variety of tags to describe and classify EEG findings and associated clinical conditions are included in the report HTML template. All tags that apply for the EEG being considered can be quickly selected directly on the report by clicking on the associated checkbox. The saved MHTML report retains the "CHECKED" flag inside each selected tag. The php script consequently parses the reports and extracts the selected tags and exports them to a CSV file for analysis with spreadsheet, database or statistical software.

Steps to make it work
1) customize test_report.htm header, logo image, layout and all you want based on your preferences
2) test_report.htm includes some tags to describe EEG findings and associated clinical conditions; you can keep those unchanged or not but remember that if you make any change to the tags, the EEGreportparser.php must be changed accordingly
3) replace the old HTML report with the new one and produce some EEG reports; for each report select all tags that apply for that EEG recording and save the report
4) when you are ready to parse, place all MHTML reports in a folder and put the folder name in $dirname variable within EEGreportparser.php (I suggest exporting reports to HIS defining a filename template suitable to easily identify each recording)
5) run EEGreportparser.php in the parent folder where $dirname is placed using PHP-CLI (command line interface) as follows:  > php EEGreportparser.php
6) while running, the scripts shows the names of the processed reports, tag names that have been selected for each report and the total number of selected tags for that report (currently output messages are in Italian). If everything goes well, in a few seconds a CSV is generated with a line for each processed report and a column for each tag. If a tag was selected, the relative cell shows "1", otherwise "0".

I suggest running the script in a GNU/Linux terminal using PHP-CLI interpreter. I have not tried running it in other operating systems or remotely on a server running PHP.

The default tags list with a brief explanation can be found in tag_legend.txt


    Copyright (C) <2020>  <Lucio Marinelli>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

To contact the author please visit the website https://www.luciomarinelli.com

