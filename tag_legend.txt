To obtain a complete and useful database I recommend to check ALL tags that apply while preparing a EEG report.

Background:
- attenuation (amplitude between 10 and 20 uV)
- suppression (amplitude <10 uV)
- 5-6Hz
- 7-8Hz
- no gradient (antero-posterior gradient is lacking)
- asymmetric
- discontinuous
- unreactive
- brain death

Epileptiform:
- generalized
- focal
- multifocal
- ictal
- interictal
- S&W (spike-and-wave, 3 Hz)
- SE (Status Epilepticus)
- NCSE (Non convulsive SE)
- EPC (epilepsia partialis continua)

Periodic discharges:
- LPD (Lateralized Periodic Discharges)
- BIPD (Bilateral Independent Periodic Discharges)
- GPD (Generalized Periodic Discharges)
- SIRPID (stimulus-induced Discharges)
- burst-suppression
- TW (Triphasic Waves)

Etiology:
- anoxic
- toxic-metabolic
- lesional
- TBI (traumatic brain injury)
- encephalitis
- dementia
- prion disease

Other:
- Normal variant (wicket spikes, 6 Hz spike and wave, 14 and 6 Hz positive bursts, SREDA or other)
- focal slowing (aspecific focal non-epileptiforn and non-periodic slowing)
- rhythmic delta activity
- EDB (Extreme Delta Brush)
- coma (select if patient is comatose)

